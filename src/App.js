import "./App.css";
import President from "./pages/President/President";
import {
  BrowserRouter,
  Navigate,
  Route,
  Routes,
  useNavigate,
} from "react-router-dom";
import State from "./pages/State/State";
import StateDetail from "./pages/StateDetail/StateDetail";
import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { getElectionData } from "./store/actions/election";
import 'react-tippy/dist/tippy.css'
import Detail from "./pages/Detail/Detail";

function App() {
  const dispatch =useDispatch()
  useEffect(() => {
    dispatch(getElectionData)
  }, []);
  
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path="/" exact element={<Navigate to="/president" />} />
          <Route path="/president" exact element={<President />} />
          <Route path="/state" exact element={<State />} />
          <Route path="/detail/:id" element={<Detail />} />

          <Route path="/stateDetail" exact element={<StateDetail />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
