import React from "react";
import { useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import style from "./DetailStyle.module.css"
export default function Detail() {
  var { id } = useParams();
  const electionList = useSelector((state) => state.election.electionData);
  return (
    <div className="container text-center">
      {electionList.filter((x) => x.id == id)
        .map((item, index) => (
          <div className="w-75 col-6 mx-auto">
          <h4 className="font-weight-bold">{item.stateName}</h4>
          <p>
            <span className="text-danger">Updated at {item.lastUpdated}</span>
            <span>99% in</span>
          </p>
          <div className="col">
            <div className={`${style.resultDetail}`} >
              <p>
                {item.candidates[0].fullName}: {item.score}
              </p>
              <p>{item.candidates[0].vote}</p>
              <p>{item.candidates[0].votePct}%</p>
            </div>
            <div className={`${style.resultDetail}`}>
              <p>
                {" "}
                {item.candidates[1].fullName}: {item.score}
              </p>
              <p> {item.candidates[1].vote}</p>
              <p> {item.candidates[1].votePct}%</p>
            </div>
          </div>
          <div className={`${style.nav} d-flex justify-content-between`}>
            <span>State Results+</span>
            <span>Country Results+</span>
            <span>Exit Poll+</span>
          </div>
        </div>
        ))}
    </div>
  );
}
