import React from "react";
import { useSelector } from "react-redux";
import Header from "../../components/Header/Header";
import DetailItem from "../../components/StateDetailItem/DetailItem";

export default function StateDetail() {
  const electionList = useSelector((state) => state.election.electionData);

  return (
    <div>
      <Header />
      <div className="container text-center">
        <h1 className="mb-5">State Detail</h1>
        <div className="state__detail__content row">
          {electionList.map((item, index) => (
            <DetailItem item={item} key={index} />
          ))}
        </div>
      </div>
    </div>
  );
}
