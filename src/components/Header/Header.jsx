import React from "react";
import { Link, NavLink } from "react-router-dom";
import style from "./HeaderStyle.module.css";
export default function Header() {
  const navNavLinkClass = ({ isActive }) => {
    return isActive ? `nav__item ${style.activeNavItem}` : "nav__item";
  };
  return (
    <header>
      <div className={`${style.header__content} text-center mt-2`}>
        <ul className="d-flex justify-content-center">
          <li>
            <NavLink
              to="/president"
              style={{ color: "black" }}
              className={navNavLinkClass}
            >
              President
            </NavLink>
          </li>
          <li>
            <NavLink style={{ color: "black" }} to="/state" className={navNavLinkClass}>
              State
            </NavLink>
          </li>
          <li>
            {" "}
            <NavLink style={{ color: "black" }} to="/stateDetail" className={navNavLinkClass}>
              State Detail
            </NavLink>
          </li>
        </ul>
      </div>
    </header>
  );
}
