import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { Tooltip } from "react-tippy";
import style from "./StateDetailItem.module.css";

export default function StateItem(props) {


  const diplayColor = () => {
    if (props.item.candidates[0].fullName.substring(0, 3) === "Don") {
      return "red";
    } else if (props.item.candidates[0].fullName.substring(0, 3) === "Joe") {
      return "blue";
    }
  };
  const navigate = useNavigate();

  function OpenDetailUser(id) {
    navigate("/detail/" + id);
  }
  const numOfDay = () => {
    const oneDay = 24 * 60 * 60 * 1000;
    const firstDay = new Date(props.item.lastUpdated);
    const secondDay = new Date();

    return Math.round(Math.abs((firstDay - secondDay) / oneDay));
  };
  return (
    <div className="col-1 w-100">
      <Tooltip
        theme="light"
        arrow
        html={
          <div style={{ width: 380, height: 210 }} className="wrap pr-2">
            <div className={style.namecode}>{props.item.stateName}</div>
            <div className={`d-flex justify-content-between ${style.infocode}`}>
              <div className="votestate font-weight-bold text-left">
                Elec.Votes: {props.item.electoralVotes}
              </div>
              <div className="update text-danger">Update: {numOfDay()} days ago</div>
              <p className="in">99% In</p>
            </div>
            <div className={`${style.ratio_state}`}>
              <div
                style={{ backgroundColor: diplayColor() }}
                className=" d-flex justify-content-between text-white align-items-center p-1  text-center"
              >
                <p>
                  {props.item.candidates[0].fullName}: {props.item.score}
                </p>
                <p>{props.item.candidates[0].vote}</p>
                <p>{props.item.candidates[0].votePct}%</p>
              </div>
              <div
                style={{ backgroundColor: diplayColor() }}
                className=" d-flex justify-content-between p-1 mt-2 text-center"
              >
                <p>
                  {" "}
                  {props.item.candidates[1].fullName}: {props.item.score}
                </p>
                <p> {props.item.candidates[1].vote}</p>
                <p> {props.item.candidates[1].votePct}%</p>
              </div>
            </div>
          </div>
        }
      >
        <button
          className="btn mt-3"
          key={props.item.id}
          onClick={() => OpenDetailUser(props.item.id)}
          style={{ backgroundColor: diplayColor() }}
        >
          <h5>{props.item.stateCode}</h5>
        </button>
      </Tooltip>
    </div>
  );
}
